import React, { useState, useRef, useEffect } from "react";
import { getWinner } from "../helpers/getWinner";
import Board from "../components/Excercise2/Board";

const initialState = [Array(9).fill(null)];

const TicTacToe = () => {
  const [history, setHistory] = useState(initialState);
  const [turn, setTurn] = useState(0);
  const [player, setPlayer] = useState("X");
  const nextRef = useRef<HTMLButtonElement | null>(null);
  const previousRef = useRef<HTMLButtonElement | null>(null);
  const resumeRef = useRef<HTMLButtonElement | null>(null);
  const winner = getWinner(history[turn]);

  const handleClick = (i: number) => {
    const historyPoint = history.slice(0, turn + 1);
    const current = historyPoint[turn];
    const cells = [...current];

    if (winner || cells[i]) return;
    cells[i] = player;
    setHistory([...historyPoint, cells]);
    setTurn(historyPoint.length);

    if (player === "X") {
      setPlayer("O");
    } else {
      setPlayer("X");
    }
  };

  const previous = () => {
    setTurn(turn - 1);

    if (player === "X") {
      setPlayer("O");
    } else {
      setPlayer("X");
    }
  };

  const next = () => {
    setTurn(turn + 1);

    if (player === "X") {
      setPlayer("O");
    } else {
      setPlayer("X");
    }
  };

  const reset = () => {
    setTurn(0);
    setPlayer("X");
    setHistory(initialState);
  };

  const resume = () => {
    setTurn(history.length - 1);
  };

  useEffect(() => {
    if (turn === 0 || turn === history.length - 1) {
      if (turn === 0) {
        if (previousRef && previousRef.current) {
          previousRef.current.disabled = true;
        }
      } else if (turn > 0) {
        if (previousRef && previousRef.current) {
          previousRef.current.disabled = false;
        }
      }

      if (turn === history.length - 1) {
        if (nextRef && nextRef.current) {
          nextRef.current.disabled = true;
        }
      }
    } else {
      if (nextRef && nextRef.current && previousRef && previousRef.current) {
        nextRef.current.disabled = false;
        previousRef.current.disabled = false;
      }
    }
  }, [turn, history.length]);

  return (
    <div className="tictactoe">
      <Board
        cells={history[turn]}
        handleClick={turn !== history.length - 1 ? () => {} : handleClick}
      />
      <div className="tictactoe-menu">
        <button ref={nextRef} onClick={next}>
          Next
        </button>
        <button ref={previousRef} onClick={previous}>
          Previous
        </button>
        <button ref={resumeRef} onClick={resume}>
          Resume
        </button>
        <button onClick={reset}>Reset</button>
        <p>Next Player: {player}</p>
        {winner ? ((player === "X") ? <p>O wins!</p>: <p>X wins!</p>) : (null)}
      </div>
    </div>
  );
};

export default TicTacToe;
