import React, { useState, useEffect, useRef } from "react";
import Board from "../components/Exercise1/Board";

const initialState = [Array(16).fill(null)];

const Squares = () => {
  const [history, setHistory] = useState(initialState);
  const [turn, setTurn] = useState(0);
  const nextRef = useRef<HTMLButtonElement | null>(null);
  const previousRef = useRef<HTMLButtonElement | null>(null);
  const resumeRef = useRef<HTMLButtonElement | null>(null);

  const handleClick = (ev: React.MouseEvent, i: number) => {
    const target = ev.target as HTMLDivElement;
    const historyPoint = history.slice(0, turn + 1);
    const current = historyPoint[turn];
    const squares = [...current];
    squares[i] = true;

    setHistory([...history, squares]);
    setTurn(history.length);

    target.className = "square focused";
  };

  useEffect(() => {
    if (turn === 0 || turn === history.length - 1) {
      if (turn === 0) {
        if (previousRef && previousRef.current) {
          previousRef.current.disabled = true;
        }
      } else if (turn > 0) {
        if (previousRef && previousRef.current) {
          previousRef.current.disabled = false;
        }
      }

      if (turn === history.length - 1) {
        if (nextRef && nextRef.current) {
          nextRef.current.disabled = true;
        }
      }
    } else {
      if (nextRef && nextRef.current && previousRef && previousRef.current) {
        nextRef.current.disabled = false;
        previousRef.current.disabled = false;
      }
    }
  }, [turn, history.length]);

  const previous = () => {
    setTurn(turn - 1);
  };

  const next = () => {
    setTurn(turn + 1);
  };

  const reset = () => {
    setTurn(0);
    setHistory(initialState);
  };

  const resume = () => {
    setTurn(history.length - 1);
  };

  return (
    <div className="squares">
      <Board
        items={history[turn]}
        handleClick={turn !== history.length - 1 ? () => {} : handleClick}
      />
      <div className="squares-menu">
        <button ref={nextRef} onClick={next}>
          Next
        </button>
        <button ref={resumeRef} onClick={resume}>
          Resume
        </button>
        <button ref={previousRef} onClick={previous}>
          Previous
        </button>
        <button onClick={reset}>Reset</button>
      </div>
    </div>
  );
};

export default Squares;
