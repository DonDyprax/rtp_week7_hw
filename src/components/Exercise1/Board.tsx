import React from "react";
import Square from "../Exercise1/Square";
import "../../styles/css/style.css";

interface Props {
  items: Array<any>;
  handleClick: (ev: React.MouseEvent, i: number) => void;
}

const Board: React.FC<Props> = ({ items, handleClick }) => {

  return (
    <div className="board-squares">
      {items.map((item, i) => {
        if (item === true) {
          return (
            <Square
              key={i}
              id={i}
              focused={true}
              onClick={(ev: React.MouseEvent) => handleClick(ev, i)}
            />
          );
        } else {
          return (
            <Square
              key={i}
              id={i}
              focused={false}
              onClick={(ev: React.MouseEvent) => handleClick(ev, i)}
            />
          );
        }
      })}
    </div>
  );
};

export default Board;
