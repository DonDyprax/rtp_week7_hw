import React from "react";
import "../../styles/css/style.css";

interface Props {
  id: number;
  onClick: (ev: React.MouseEvent) => void;
  focused: boolean;
}

const Square: React.FC<Props> = ({ id, onClick, focused }) => {
  const style = focused ? "square focused" : "square";

  return <div className={style} onClick={onClick}></div>;
};

export default Square;
