import React, { useState, useCallback } from "react";
import Squares from "../pages/Squares";
import TicTacToe from "../pages/TicTacToe";
import "../styles/css/style.css";
import Navbar from "../components/common/Navbar";

const App = () => {
  const [page, setPage] = useState("Squares");

  const pages: any = {
    Squares: <Squares />,
    TicTacToe: <TicTacToe />,
  };

  const changeCurrentPage = useCallback((ev, pageName) => {
    ev.preventDefault();
    setPage(pageName);
  }, []);

  return (
    <div className="app">
      <Navbar setPage={changeCurrentPage} />
      {pages[page]}
    </div>
  );
};

export default App;
