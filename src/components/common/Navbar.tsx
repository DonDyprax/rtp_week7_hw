import React from "react";

interface Props {
  setPage: (ev: React.MouseEvent, pageName: string) => void;
}

const Navbar: React.FC<Props> = ({ setPage }) => {
  return (
    <div className="navbar">
      <div className="links">
        <ul>
          <li>
            <a href="/" onClick={(ev) => setPage(ev, "Squares")}>
              {" "}
              Squares
            </a>
          </li>
          <li>
            <a href="/" onClick={(ev) => setPage(ev, "TicTacToe")}>
              {" "}
              TicTacToe
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Navbar;
