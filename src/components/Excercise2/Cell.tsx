import React from "react";

interface Props {
  value: string;
  handleClick: () => void;
}

const Cell: React.FC<Props> = ({ value, handleClick }) => {
  const style = value ? `cell ${value}` : `cell`;

  return (
    <div className={style} onClick={handleClick}>
      {value}
    </div>
  );
};

export default Cell;
