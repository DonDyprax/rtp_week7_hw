import React from "react";
import Cell from "./Cell";

interface Props {
  cells: Array<string>;
  handleClick: (index: number) => void;
}

const Board: React.FC<Props> = ({ cells, handleClick }) => {
  return (
    <div className="board-tictactoe">
      {cells.map((cell, i) => (
        <Cell key={i} value={cell} handleClick={() => handleClick(i)} />
      ))}
    </div>
  );
};

export default Board;
