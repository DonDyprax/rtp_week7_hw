import { useEffect, useRef } from "react";

function usePrevious<T>(value: T): [T[], () => T] {
  const ref = useRef<Array<T>>([]);

  const getPreviousValue = () => {
    return ref.current[ref.current.length - 2];
  };

  useEffect(() => {
    ref.current.push(value);
  }, [value]);

  return [ref.current, getPreviousValue];
}

export default usePrevious;